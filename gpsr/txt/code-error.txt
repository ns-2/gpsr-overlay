int *
GPSRAgent::nexthopVAP(int index, nsaddr_t src, nsaddr_t dst) {
  int result[2];

  // jika jarak destination terjangkau
  MobileNode * dstNode= (MobileNode * )(Node::get_node_by_address(dst));
  if ( nblist_->getdis( my_x_, my_y_, dstNode->X(), dstNode->Y()) < RANGE_TRANSMISSION ) {
    #ifdef DEBUG
      fp= fopen ("debug.txt", "a");
      fprintf (fp, "%f %s function, line %d ,file %s, dst dekat\n", GPSR_CURRENT, __FUNCTION__, __LINE__, __FILE__);
      fclose (fp);
    #endif
    result[0]= dst;
    result[1]= index;
    return result;
  }
  // jika index > daripada jumlah vap
  if ( index > vt->vt_count(src, dst) ) {
    #ifdef DEBUG
      fp= fopen ("debug.txt", "a");
      fprintf (fp, "%f %s function, line %d ,file %s, index kebanyakan\n", GPSR_CURRENT, __FUNCTION__, __LINE__, __FILE__);
      fclose (fp);
    #endif
    result[0]= dst;
    result[1]= index;
    return result;
  }
  // jika jarak vap terdekat terjangkau, maka pilih vap selanjutnya
  if ( nblist_->getdis( my_x_, my_y_, vt->vt_getXbyIndex(src, dst, index), vt->vt_getYbyIndex(src, dst, index) ) < RANGE_TRANSMISSION ) {
    #ifdef DEBUG
      fp= fopen ("debug.txt", "a");
      fprintf (fp, "%f %s function, line %d ,file %s, next vap\n", GPSR_CURRENT, __FUNCTION__, __LINE__, __FILE__);
      fclose (fp);
    #endif
    index++;
  }

  // todo gf_nexthop ganti pake paper momon
  nsaddr_t nexthop;
  nexthop= nblist_->gf_nexthop(vt->vt_getXbyIndex(src, dst, index), vt->vt_getYbyIndex(src, dst, index));
  #ifdef DEBUG
    fp= fopen ("debug.txt", "a");
    fprintf (fp, "%f %s function, line %d ,file %s, nexthop %d\n", GPSR_CURRENT, __FUNCTION__, __LINE__, __FILE__, nexthop);
    fclose (fp);
  #endif
  while (nexthop < 0){ // tidak ada nexthop
    if (nexthop >= 0 || index > vt->vt_count(src, dst)) break;
    // cari nexthop ke vap selanjutnya
    index++;
    nexthop= nblist_->gf_nexthop(vt->vt_getXbyIndex(src, dst, index), vt->vt_getYbyIndex(src, dst, index));
    #ifdef DEBUG
      fp= fopen ("debug.txt", "a");
      fprintf (fp, "%f %s function, line %d ,file %s, loop find nexthop %d\n", GPSR_CURRENT, __FUNCTION__, __LINE__, __FILE__, nexthop);
      fclose (fp);
    #endif
  }

  result[0]= nexthop;
  result[1]= index;
  return result;
}

=== dipanggil ===

int * res;
res= nexthopVAP(op->index, iph->saddr(), iph->daddr());
#ifdef DEBUG
  fp= fopen ("debug.txt", "a");
  fprintf (fp, "%f nexthop : %d, index %d\n", GPSR_CURRENT, res[0], res[1]);
  fclose (fp);
#endif
nexthop= res[0];
op->index=res[1];

MobileNode * nextNode= (MobileNode * )(Node::get_node_by_address(nexthop));

coba lagi