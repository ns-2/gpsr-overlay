#!/bin/bash

# declare input
if [ -z "$1" ]; then
  echo "Please Enter Number of Node"
  exit 1
else
  nn=$1
fi
if [ -z "$2" ]; then
  echo "Please Enter FolderPath"
  exit 1
else
  WORKPATH=$2
fi

# declare variable
node_gen=$(/usr/bin/expr $nn - 2);   # node generate
node_src=$(/usr/bin/expr $nn - 2);   # node sender
node_dst=$(/usr/bin/expr $nn - 1);   # node receiver

node_src_X=505.12;  # was 174.73
node_src_Y=1.41;    # was 101.1
node_src_Z=0

node_dst_X=798.31;  # was 603.05
node_dst_Y=295.19;  # was 607.3
node_dst_Z=0

# export SUMO_HOME (needed)
SUMO_HOME=/usr/share/sumo
export SUMO_HOME=$SUMO_HOME

# print status
printf "building scenario node=$node_gen grid\n"

# generate grid scenario
if [ -f $(pwd)/../src/map-aldo.net.xml ]; then
  /bin/cp $(pwd)/../src/map-aldo.net.xml $WORKPATH/map.net.xml
else
  /usr/bin/netgenerate -g --grid.number=9 --grid.length=100 --default.speed=15 --tls.guess=1 --output-file=$WORKPATH/map.net.xml
fi


# create random trips
/usr/bin/python /usr/share/sumo/tools/randomTrips.py -n $WORKPATH/map.net.xml -e $node_gen -l --trip-attributes="departLane=\"best\" departSpeed=\"max\" departPos=\"random_free\"" --output-trip-file=$WORKPATH/trips.trips.xml

# create route for trips and scenario
/usr/bin/duarouter -n $WORKPATH/map.net.xml -t $WORKPATH/trips.trips.xml -o $WORKPATH/route.rou.xml --ignore-errors --repair

# make a rule time start and stop
/usr/bin/sumo -b 0 -e 200 -n $WORKPATH/map.net.xml -r $WORKPATH/route.rou.xml --fcd-output $WORKPATH/scenario.xml

# export to activity and mobility file
/usr/bin/python /usr/share/sumo/tools/traceExporter.py --fcd-input=$WORKPATH/scenario.xml --ns2mobility-output $WORKPATH/map.mobility.tcl

# add source and recv node to scenario
/bin/mv $WORKPATH/map.mobility.tcl $WORKPATH/old.txt
echo '$node_('$node_src') set X_ '$node_src_X'\n$node_('$node_src') set Y_ '$node_src_Y'\n$node_('$node_src') set Z_ '$node_src_Z'\n$node_('$node_src') color cyan\n$node_('$node_dst') set X_ '$node_dst_X'\n$node_('$node_dst') set Y_ '$node_dst_Y'\n$node_('$node_dst') set Z_ '$node_dst_Z'\n$node_('$node_dst') color red\n$ns_ at 0.0 "$node_('$node_src') color cyan"\n$ns_ at 0.0 "$node_('$node_dst') color red"' > $WORKPATH/tmp.txt
(/bin/cat $WORKPATH/tmp.txt && /bin/cat $WORKPATH/old.txt) > $WORKPATH/map.mobility.tcl
/bin/rm $WORKPATH/tmp.txt $WORKPATH/old.txt
/bin/sed -i 's/-//g' $WORKPATH/map.mobility.tcl

# remove unused file
if [ -f $WORKPATH/map.net.xml ]; then
  /bin/rm $WORKPATH/map.net.xml
fi
if [ -f $WORKPATH/route.rou.alt.xml ]; then
  /bin/rm $WORKPATH/route.rou.alt.xml
fi
if [ -f $WORKPATH/route.rou.xml ]; then
  /bin/rm $WORKPATH/route.rou.xml
fi
if [ -f $WORKPATH/trips.trips.xml ]; then
  /bin/rm $WORKPATH/trips.trips.xml
fi
if [ -f $WORKPATH/scenario.xml ]; then
  /bin/rm $WORKPATH/scenario.xml
fi
