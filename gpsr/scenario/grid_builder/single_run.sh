#!/bin/bash

# declare input
if [ -z "$1" ]; then
  echo "Please Enter Number of Node"
  exit 1
else
  nn=$1
fi

# declare variable
PATH=$(pwd)
WORKPATH=$PATH/../../simulation/$nn-grid

# make folder
if [ -d $WORKPATH ]; then
  /bin/rm -rf $WORKPATH
fi
/bin/mkdir -p $WORKPATH

# prepare grid scenario
/bin/sh $PATH/prepare_scenario.sh $nn $WORKPATH

# prepare cbr
/bin/sh $PATH/prepare_cbr.sh $nn $WORKPATH

# prepare script
/bin/sh $PATH/prepare_script.sh $nn $WORKPATH

# enter folder
cd $WORKPATH

# run
/usr/local/bin/ns-keliu-mod $WORKPATH/script.tcl
# /usr/local/bin/ns-keliu-ori $WORKPATH/script.tcl

# get result
/bin/sh $PATH/../result/get_awk_result.sh gpsr_tracefile.tr $WORKPATH

# delete trace and nam file
# /bin/rm $WORKPATH/*.tr
# /bin/rm $WORKPATH/*.nam
