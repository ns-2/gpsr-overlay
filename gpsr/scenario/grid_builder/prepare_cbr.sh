#!/bin/bash

# declare input
if [ -z "$1" ]; then
  echo "Please Enter Number of Node"
  exit 1
else
  nn=$1
fi
if [ -z "$2" ]; then
  echo "Please Enter FolderPath"
  exit 1
else
  WORKPATH=$2
fi

# declare variable
path=$(pwd)
node_src=$(/usr/bin/expr $nn - 2);   # node sender
node_dst=$(/usr/bin/expr $nn - 1);   # node receiver

# create cbr
/bin/cp $path/../src/template_cbr.tcl $WORKPATH/cbr.tcl
/bin/sed -i 's/NODESRC_PLACEHOLDER/'$node_src'/g' $WORKPATH/cbr.tcl;
/bin/sed -i 's/NODEDST_PLACEHOLDER/'$node_dst'/g' $WORKPATH/cbr.tcl;
