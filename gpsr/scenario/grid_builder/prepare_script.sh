#!/bin/bash

# declare input
if [ -z "$1" ]; then
  echo "Please Enter Number of Node"
  exit 1
else
  nn=$1
fi
if [ -z "$2" ]; then
  echo "Please Enter FolderPath"
  exit 1
else
  WORKPATH=$2
fi

# declare variable
path=$(pwd)

# create script
/bin/cp $path/../src/gpsr.tcl $WORKPATH/gpsr.tcl
/bin/cp $path/../src/template_script.tcl $WORKPATH/script.tcl
/bin/sed -i 's/X_PLACEHOLDER/802/g' $WORKPATH/script.tcl;
/bin/sed -i 's/Y_PLACEHOLDER/802/g' $WORKPATH/script.tcl;
/bin/sed -i 's/IFQ_PLACEHOLDER/512/g' $WORKPATH/script.tcl;
/bin/sed -i 's/SEED_PLACEHOLDER/0.0/g' $WORKPATH/script.tcl;
/bin/sed -i 's/PROTOCOL_PLACEHOLDER/gpsr/g' $WORKPATH/script.tcl;
/bin/sed -i 's/NODE_PLACEHOLDER/'$nn'/g' $WORKPATH/script.tcl;

/bin/sed -i 's#CBRFILE_PLACEHOLDER#"./cbr.tcl"#g' $WORKPATH/script.tcl;
/bin/sed -i 's#SCEN_PLACEHOLDER#"./map.mobility.tcl"#g' $WORKPATH/script.tcl;

/bin/sed -i 's/STOP_PLACEHOLDER/200/g' $WORKPATH/script.tcl;
/bin/sed -i 's/OUTPUT_PLACEHOLDER/gpsr_tracefile.tr/g' $WORKPATH/script.tcl;
/bin/sed -i 's/NAM_PLACEHOLDER/gpsr_tracefile.nam/g' $WORKPATH/script.tcl;

/bin/sed -i 's#NSPATH_PLACEHOLDER#"/root/ns2/ns-allinone-2.35-keliu-mod/ns-2.35"#g' $WORKPATH/script.tcl;

/bin/sed -i 's/PLANAR_PLACEHOLDER/1/g' $WORKPATH/script.tcl;
/bin/sed -i 's/HELLO_PLACEHOLDER/1.0/g' $WORKPATH/script.tcl;
