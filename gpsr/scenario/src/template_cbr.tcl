# GPSR routing agent settings
for {set i 0} {$i < $opt(nn)} {incr i} {
    $ns_ at 0.00002 "$ragent_($i) turnon"
    $ns_ at 3.0 "$ragent_($i) neighborlist"
#    $ns_ at 149.0 "$ragent_($i) turnoff"
#	$ns_ at 80 "$ragent_($i) turnon"
}

# Source will start sink
$ns_ at 0.00002 "$ragent_(NODESRC_PLACEHOLDER) startSink 10.0"
$ns_ at 0.00002 "$ragent_(NODEDST_PLACEHOLDER) startSink 10.0"
#$ns_ at 11.0 "$ragent_(9) startSink 0.0"


# Upper layer agents/applications behavior
set udp_(0) [new Agent/UDP]
$ns_ attach-agent $node_(NODESRC_PLACEHOLDER) $udp_(0)

set null_(0) [new Agent/Null]
$ns_ attach-agent $node_(NODEDST_PLACEHOLDER) $null_(0)

set cbr_(0) [new Application/Traffic/CBR]
$cbr_(0) set packetSize_ 32
$cbr_(0) set interval_ 1.0
$cbr_(0) set random_ 1
#    $cbr_(0) set maxpkts_ 100
$cbr_(0) attach-agent $udp_(0)
$ns_ connect $udp_(0) $null_(0)
$ns_ at 10.0 "$cbr_(0) start"
$ns_ at 195.0 "$cbr_(0) stop" 
