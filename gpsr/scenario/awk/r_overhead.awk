BEGIN{
	recvs = 0;
	routing_packets = 0;
}

{
	if(($1 == "r") && ($7 == "cbr")) 
		recvs++;
	if(($1 == "s" || $1 == "r") && ($4 == "RTR") && ($7 == "gpsr"))
		routing_packets++;
}

END{
	printf("\nrouting packets: %d", routing_packets);
	printf("\ndata = %d", recvs);
	printf("\noverhead : %.3f\n", routing_packets/recvs);
}