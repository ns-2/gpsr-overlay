BEGIN {
	sendLine = 0;
	recvLine = 0;
	forwardLine = 0;
}

$0~/^s.*AGT/{
	sendLine++
}

$0~/^r.*AGT/{
	recvLine++
}

$0~/^f.*RTR/{
	forwardLine++
}

END{
	printf"Ratio:%.4f\n",(recvLine/sendLine)*100;
}