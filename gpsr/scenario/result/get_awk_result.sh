#!/bin/bash

# declare input
if [ -z "$1" ]; then
  echo "Please Enter Trace File Name"
  exit 1
else
  TR_FILENAME=$1
fi
if [ -z "$2" ]; then
  echo "Please Enter FolderPath"
  exit 1
else
  WORKPATH=$2
fi

# check is tr file exist
if [ ! -f "$WORKPATH/$TR_FILENAME" ]; then
  echo "Trace file not exists"
  exit 1
fi

# declare input and variable
PATH=$(pwd)
TR_FILE=$WORKPATH/$TR_FILENAME

if [ -d "$PATH/../awk" ]; then
  awkfolder=$PATH/../awk
else
  awkfolder=$PATH/../../scenario/awk;
fi

if [ -d "$awkfolder" ]; then
  /bin/cp $awkfolder/*.awk $WORKPATH
else
  echo "awk folder ($awkfolder) not exists"
  exit 1
fi

# run awk and save to txt
/usr/bin/awk -f $WORKPATH/e2e.awk $TR_FILE > $WORKPATH/e2e.txt
/usr/bin/awk -f $WORKPATH/PDR.awk $TR_FILE > $WORKPATH/PDR.txt
/usr/bin/awk -f $WORKPATH/pdr.awk $TR_FILE > $WORKPATH/pdr.txt
/usr/bin/awk -f $WORKPATH/r_overhead.awk $TR_FILE > $WORKPATH/r_overhead.txt

# print result
printf "\nshowing result of tr file $TR_FILENAME on $WORKPATH\n"

printf "\nend to end delay\n"
/bin/cat $WORKPATH/e2e.txt

printf "\nPacket delivery ratio\n"
/bin/cat $WORKPATH/pdr.txt

printf "\nPacket delivery ratio\n"
/bin/cat $WORKPATH/PDR.txt

printf "\nRouting Overhead packet\n"
/bin/cat $WORKPATH/r_overhead.txt

# delete awk and txt file
/bin/rm $WORKPATH/*.awk
/bin/rm $WORKPATH/r_overhead.txt $WORKPATH/e2e.txt $WORKPATH/pdr.txt
